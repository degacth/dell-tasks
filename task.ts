/// TEST: find design errors in that code

class NamedObject {
    name: string;

    getFullName(): string {
        if (this instanceof Product) return 'Product: ' + this.name;
        if (this instanceof Category) return 'Category: ' + this.name;
        else return 'Unknown: ' + this.name;
    }

    logNames(objs: (Product | Category)[]): void {
        for (var i; i < objs.length; i++)
            console.log(objs[i].getFullName())
    }
}

class Category extends NamedObject {
    id: number;
    products: Product[];
}

class Product extends NamedObject {
    id: number;
    price: number;
    category: Category;

    static getFullPrice(products: Product[]): number {
        var sum = 0;
        for (var i = 0; i < products.length; i++) sum += products[i].price;
        return sum;
    }
}

class Cart {
    products: Product[];

    getDiscount(): number {
        var fullPrice = Product.getFullPrice(this.products);
        var discount: number;

        if (fullPrice > 500) {
            discount = fullPrice * 0.1;
        }
        else if (fullPrice > 200) {
            discount = fullPrice * 0.05
        }
        else {
            discount = 0;
        }
        return discount;
    }
}
