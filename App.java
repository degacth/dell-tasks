import java.io.PrintStream;

class App {
    static PrintStream o = System.out;
    private boolean foo;
    private boolean bar;
    private int hello = -1;
    private int world = -1;

    public static void main(String[] args) throws Exception {
        int maxSteps = 100000;
        for (int step = 0; step < maxSteps; step++) {
            App app = new App();

            Thread thread1 = new Thread(app::updateWorld);
            Thread thread2 = new Thread(app::updateHello);

            thread1.start();
            thread2.start();

            try {
                thread1.join();
                thread2.join();
            } catch (InterruptedException e) {
                o.println(e.toString());
            }

            if (app.hello == 1 && app.world == 1) {
                throw new Exception("oh, fuck, iteration: " + step);
            }
        }
    }

    private void updateWorld() {
        foo = true;
        world = bar ? 0 : 1;
        o.println("world = " + world);
    }

    private void updateHello() {
        bar = true;
        hello = foo ? 0 : 1;
        o.println("hello = " + hello);
    }
}
